# mumosic

This is a module for mumo.

It's able to get some music from any source supported by youtube-dl and play it

His particularity is that each users can spawn a bot for playing music in his channel (yeah, even simultaneously

## Usage

By default use !play $URL for playing a sound (see mumosic.ini)

It will spawn a new user named _musicbot_$USER, with $USER replaced with user name (the one wich used !play)

Until the user disconnect/stop it (via !stop) the bot, for now, stay connected and continue to play sound (only his user can add new sound)

If allowed in ini, you can spawn a bot in another channel than your.

You can restrict who can spawn bot.

You can also make some channels restricted, where only specified user can spawn and control a bot.

Since for now we use the pymumble client we need the server password in the ini (until we find another way)


