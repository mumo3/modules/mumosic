#!/usr/bin/env python
# -*- coding: utf-8

from mumo_module import (commaSeperatedIntegers,
                         commaSeperatedBool,
                         commaSeperatedStrings,
                         MumoModule)
import concurrent.futures
import json
import pymumble_py3
import re
import subprocess as sp
import time
import wave
import youtube_dl

class mumosic(MumoModule):
    default_config = {'mumosic':(
                                ('servers', commaSeperatedIntegers, []),
                                ('keyword', str, "!play"),
                                ('allow_multiple', bool, True),
                                ('allow_unregistered', bool, False),
                                ('allow_bombing', bool, False),
                                ('allowed_user', commaSeperatedStrings, []),
                                ('restricted_channels', str, ""),
                                ('server_password', str, ""),
                                ),
                    }
    def __init__(self, name, manager, configuration = None):
        MumoModule.__init__(self, name, manager, configuration)
        self.murmur = manager.getMurmurModule()
        self.data = {}
        self.players = {}

    def isregistered(self, userid):
        if (userid == -1):
            return False
        else:
            return True

    def connected(self): 
        manager = self.manager()
        log = self.log()
        log.debug("Register for Server callbacks")

        servers = self.cfg().mumosic.servers
        if not servers:
            servers = manager.SERVERS_ALL

        manager.subscribeServerCallbacks(self, servers)

    def disconnected(self): pass

    def is_allowed(self, user, channel):
        multiple = self.cfg().mumosic.allow_multiple
        bombing = self.cfg().mumosic.allow_bombing
        allowed_user = self.cfg().mumosic.allowed_user
        restricted_channels = json.loads(self.cfg().mumosic.restricted_channels)

        if not multiple:
            self.log().error(self.players.keys())
            if len(list(self.players.keys()))>=1:
                return False

        if restricted_channels:
            if channel in [int(x) for x in restricted_channels.keys()]:
                if user.name not in restricted_channels[str(channel)]:
                    return False
        if allowed_user[0] != ''  and (user.name not in allowed_user):
            return False
        return True

    def spawn_player(self, user, message):
        self.players[user.name]["player"].set_receive_sound(1)
        self.players[user.name]["player"].start()
        self.players[user.name]["player"].is_ready()
        tmp  = self.players[user.name]["player"]
        if self.cfg().mumosic.allow_bombing:
            tmp.users.myself.move_in(message.channels[0])
        else:
            tmp.users.myself.move_in(user.channel)




    def download_track(self, link):

        ydl_opts = {
                    'format': 'bestaudio/best',
                    'outtmpl': '/tmp/%(title)s.%(ext)s',
                    'postprocessors': [{
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'wav',
                        'preferredquality': '192',
                    }],
                    'postprocessor_args': [
                        '-ac', "1"
                    ],
                   }
        filename = ""
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            i = 0
            try:
               info_dict = ydl.extract_info(link, download=True)
            except:
                info_dict = {}
            while not info_dict and i < 10:
                time.sleep(0.5)
                try:
                    info_dict = ydl.extract_info(link, download=True)
                except Exception as e:
                    i += 1
                    if i >= 10:
                        raise e
            filename = ydl.prepare_filename(info_dict).split(".")
            filename = filename[0] + ".wav"
        return filename

    def play_tracks(self, user):
        to_play = self.players[user.name]['to_play']
        played = self.players[user.name]['played']
        player = self.players[user.name]['player']
        for m in to_play:
            f = m.result()
            played.append(m)
            to_play.remove(m)

            with wave.open(f, 'rb') as sound:
                to_play = sound.readframes(sound.getnframes())
                player.sound_output.add_sound(to_play)

        return True

    def userTextMessage(self, server, user, message, current=None): 
        if message.text.startswith("!help"):
            server.sendMessageChannel(user.channel, False, """<br>You can spawn a music bot for playing sound from an url
                                                              <br>!play $URL : play/add an URL to your music bot (URL can be whatever youtube-dl support)
                                                              <br>!stop Kill your bot""" )

        if message.text.startswith("!stop"):
            player = self.players.get(user.name, {}).get('player', None)
            if player:
                player.stop()
                del self.players[user.name]

        if message.text.startswith(self.cfg().mumosic.keyword):
            if self.is_allowed(user,message.channels[0]):
                msg = message.text[len(self.cfg().mumosic.keyword):].strip()
                msg =re.findall('a href=\"(.*)\"', msg)[0]

                if user.name not in self.players:
                    self.players[user.name] = {
                            "links": [],
                            "to_play": [],
                            "played": [],
                            "player": pymumble_py3.Mumble("127.0.0.1", f"_musicbot_{user.name.replace(' ','_')}", password=self.cfg().mumosic.server_password) 
                            }
                    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
                        executor.submit(self.spawn_player, user, message)
                links = self.players[user.name]['links']
                to_play = self.players[user.name]['to_play']
                played = self.players[user.name]['played']
                player = self.players[user.name]['player']
                ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s.%(ext)s'})
    #            with ydl:
    #                i = 0
    #                try:
    #                    result = ydl.extract_info(msg,download=False)
    #                except:
    #                    result = False
    #                while not result and i < 10:
    #                    try:
    #                        time.sleep(0.5)
    #                        result = ydl.extract_info(msg,download=False)
    #                    except:
    #                        i += 1
    #            if 'entries' in result:
    #                for i in result['entries']:
    #                    if i not in links:
    #                        links.append(i)
    #            elif i<10:
                links.append(msg)
                for l in links:
                    links.remove(l)
                    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
                        t = executor.submit(self.download_track, l)
                        to_play.append(t)

                with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
                    t = executor.submit(self.play_tracks, user)

    #            if (to_play and played) and (to_play == played):
    #                frames = sound.getnframes()
    #                rate = sound.getframerate()
    #                duration = frames / float(rate)
    #                time.sleep(int(float(duration)+2))
    #            while player.sound_output.get_buffer_size() > 0.0:
    #                time.sleep(1)
    #            player.stop()
    #            del self.players[user.name]





        


    def userConnected(self, server, state, context = None): pass
    def userDisconnected(self, server, state, context = None):
        a = self.players.get(state.name, {}).get("player",None)
        if a:
            a.stop()

    def userStateChanged(self, server, state, context = None):
        a = self.players.get(state.name, {}).get("player",None)
        if a:
            if a.sound_output.get_buffer_size() == 0.0:
                a.stop()
    def channelCreated(self, server, state, context = None): pass
    def channelRemoved(self, server, state, context = None): pass
    def channelStateChanged(self, server, state, context = None): pass

    def __getattr__(self, item):
        """This is a bypass method for getting rid of all the callbacks that are not handled by this module.

        :param item: item that is being queried for
        :return: an unused callable attribute that does not do anything
        """
        def unused_callback(*args, **kwargs):
            pass
        return unused_callback

